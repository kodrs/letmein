var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var sendResponseWithToken = require('../utils/jwt.js');

passport.use('local-register', new LocalStrategy(function(username, password,done){
	//find the user in the database
	User.findOne({username:username},function(err,user){
		//In case of error
		if(err){
			return done(err);
		}
		//If user is found - user already exists
		if(user){
			console.log(user);
			return done(null, false,{message: "User exists"});
		}
		//save user
		var newUser = new User({
			username:username,
			password:password
		});
		newUser.save(function(err){
			done(null,newUser);
		});
	});
}));

router.post('/',passport.authenticate('local-register'),function(req,res){
	//res.send("user registered");
	sendResponseWithToken(req.user,res);
});

module.exports = router;
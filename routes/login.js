var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var router = express.Router();
var User = require('../models/user.js');
var sendResponseWithToken = require('../utils/jwt.js');

/**
 * Local Strategy for Passport Authentication
 */
passport.use('local-login', new LocalStrategy({usernameField: 'username'}, function (username, password, done) {
    var searchUser = {
        username: username
    };
    User.findOne(searchUser, function (err, user) {
        if (err) {
            return done(err);
        }

        if (!user) {
            return done(null, false, {message: "No user found"})
        }

        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                return done(err)
            };
            if (!isMatch) {
                return done(null, false, {message: "Wrong Password"})
            };

            return done(null, user);
        });

    });
}));

router.post('/', passport.authenticate('local-login'), function (req, res) {
    sendResponseWithToken(req.user, res);
});

module.exports = router;
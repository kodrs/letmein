var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var SECRET = require('./config').SECRET;

/**
 * requiring modules for passport
 */
var passport = require('passport');
var mongoose = require('mongoose');
var users = require('./routes/users');
var login = require('./routes/login');
var register = require('./routes/register');
//var User = require('./models/user.js');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Setting up passport
 */
app.use(passport.initialize());
passport.serializeUser(function(user,done){
   done(null, user.id); 
});

/**
 * Cross Origin Policy
 */

app.use(function(req,res,next){
   res.header('Access-Control-Allow-Origin','*');
   res.header('Access-Control-Allow-Credentials','true');
   res.header('Access-Control-Allow-Methods','*');
   res.header('Access-Control-Allow-Headers','Content-Type,Authorization'); 
   next();
});


//app.use('/', routes);
app.use('/users', users);
app.use('/login',login);
app.use('/register',register);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

/**
 * Database configuration and connection
 */
var dbConfig = 'mongodb://localhost:27017/letmein';
mongoose.connect(dbConfig,{db:{safe:true}},function(err,res){
    if(err){
        console.log("Error connecting to DB");
    }
    else{
        console.log("connected to .. "+dbConfig);
    }
});

module.exports = app;

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new mongoose.Schema({
	username:String,
	password:String
});

/**
 * This will remove the password from the user object
 * So that the response will not have the password
 */
UserSchema.methods.toJSON = function(){
	var user = this.toObject();
	delete user.password;
	return user;	
};

UserSchema.methods.comparePassword = function(password, callback){
	/**
	 * This is for simple string passwords comparision
	 */
	/*if(password === this.password){
		callback(null,true);
	}
	else{
		callback({message:"wrong password"},false);	
	}*/
	
	/**
	 * The below bcrypt compare allows us to compare the encrypted passwords
	 */
	bcrypt.compare(password, this.password, callback);
	
};

UserSchema.pre('save',function(next){
	//logic to encrypt password and store the password representation in DB.
	var user = this;
	
	/**
	 * Storing the encrypted passwords using bcrypt.
	 */
	bcrypt.genSalt(10,function(err,salt){
		if(err){return next(err);};
		bcrypt.hash(user.password,salt,null,function(err,hash){
			if(err){return next(err);};
			user.password = hash;
			next();
		});
	});
});

module.exports = mongoose.model('User', UserSchema);